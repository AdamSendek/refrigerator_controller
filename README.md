# Refrigerator_Controller
Last update: 29-03-2020

## Description
Refrigerator controller to maintain the temperature with signaling that the extreme temperature has been exceeded.

## Hardware
* ATtiny25                        1pcs  
* Diode LED Red                   1pcs  
* Rezistor 220 ohm                1pcs  
* Rezistor 4.7k ohm               1pcs  
* Rezistor 10k ohm                1pcs  
* Capacitor 47nF                  1pcs  
* Capacitor 100nF                 1pcs  
* Thermistor NTC 10k ohm          1pcs  
* Relay module                    1pcs  
* AC-DC converter - 230V to 5V    1pcs  


## Software



## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0
