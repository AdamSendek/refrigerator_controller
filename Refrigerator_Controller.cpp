#include <avr/io.h>
#include <util/delay.h>

volatile float t=0;

int main(){
ADCSRA=0b11000111;
ADMUX=0b00000001;

  DDRB = 0b00010010;//PB4 - Kontaktron, PB2 - Termistor, PB1 - Led
  PORTB = 0b00000000;

while(1){

  int temp;
  if (!(ADCSRA & (1<<ADSC))){
    temp=ADC;
    t = 1.0/((log((4750.0/((1023.0/temp)-1.0))/5500)/3200.0)+(1.0/(21 + 273.15)))-273.15;   //3200//3950
    ADCSRA |= (1<<ADSC);
  }
 
  //float t;
  //t = 1.0/((log((4750.0/((1023.0/temp)-1.0))/5500)/3200.0)+(1.0/(21 + 273.15)))-273.15;   //3200//3950
  
  if(t>=-5.0){
    PORTB&=~(1<<4);
  }else{
    if(t<=-8.0){
      PORTB|=(1<<4);
    }
  }

  if(t>=-1.0){
    PORTB|=(1<<1);//ERROR
  }else{
    PORTB&=~(1<<1);
  }
  
  _delay_ms(500);
}
return 0;
}